# Parameters to Workspace


__This component enables a Research Cloud catalog-developer to preserve workspace parameters to the disk of the running workspace.__


### Usage

- Place this component right before the respective "external" component of the catalog item
- In the catalog item, overwrite the parameter "parameters_to_save" with a space-separated list of (existing!) keys of the parameters that you want preserved.
- after this component has run, the *values* of the specified parameters can be found in the files /var/src/saved_parameters/*parameter-key*
